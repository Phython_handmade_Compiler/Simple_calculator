__author__ = 'Mahdi'
import re
tokenizer_temporary = ""
tokenizer_state = "start"

''''''


def tokenizer(char):
    global tokenizer_state
    global tokenizer_temporary



class teached_tokenizer():
    remained_file = ''
    last_token=None
    stack =[]

    def __init__(self):
        teached_tokenizer.read_file(self)

    def read_file(self):

        # Open a file
        # fo = open("foo.txt", "r") #https://www.tutorialspoint.com/python/python_files_io.htm
        self.remained_file= ""
        with open("input.txt", "r") as ins:

            for line in ins:
                self.remained_file+=(line)
        self.remained_file += "\n$"
        print("file :", self.remained_file)

    def read_token(self):
        tokenizer_temporary=""
        tokenizer_state="start"

        index=0
        type="not done"
        while(tokenizer_state!="end"):
            char = self.remained_file[index]
            if tokenizer_state == "start":

                if char == '+':
                    type = "operator"
                    tokenizer_temporary = "+"
                    tokenizer_state="end"

                elif char == '-':
                    type = "operator"
                    tokenizer_temporary = "-"
                    tokenizer_state="end"

                elif char == '*':
                    type = "operator"
                    tokenizer_temporary = "*"
                    tokenizer_state="end"

                elif char == '/':
                    type = "operator"
                    tokenizer_temporary = "/"
                    tokenizer_state="end"

                elif char == '^':
                    type = "operator"
                    tokenizer_temporary = "^"
                    tokenizer_state="end"

                elif char == '%':
                    type = "operator"
                    tokenizer_temporary = "%"
                    tokenizer_state="end"

                elif char == '(':
                    type = "("
                    tokenizer_temporary = "("
                    tokenizer_state="end"

                elif char == ')':
                    type = ")"
                    tokenizer_temporary = ")"
                    tokenizer_state="end"

                elif char == '=':
                    type = "operator"
                    tokenizer_temporary = "="
                    tokenizer_state="end"

                elif char == '$':
                    type = "EOF"
                    tokenizer_temporary = "$"
                    tokenizer_state="end"
                elif char == '\n':
                    type = "newline"
                    tokenizer_temporary = "\n"
                    tokenizer_state="end"

                elif char == ' ':
                    pass

                elif char.isdigit() and 9 >= int(char) >= 0:
                    tokenizer_temporary += char
                    tokenizer_state = "int"

                elif re.match("[a-zA-Z]",char) != None:
                    tokenizer_temporary += char
                    tokenizer_state="variable"

                elif char=='"':
                    tokenizer_state="string"

                elif char=='#':
                    tokenizer_state="comment"

                else:
                    return "unsupported char"
            elif tokenizer_state == "int":
                if  char.isdigit() and 9 >= int(char) >= 0:
                    tokenizer_temporary += char
                    tokenizer_state = "int"

                elif char == '.':
                    tokenizer_state = "real"
                    tokenizer_temporary += char

                else:
                    # restore one last character red
                    index-=1
                    type="int"
                    tokenizer_state = "end"

            elif tokenizer_state == "real":
                if 9 >= int(char) >= 0:
                    tokenizer_temporary += char

                else:
                    # restore one last character red
                    index-=1
                    type="real"
                    tokenizer_state = "end"

            elif tokenizer_state=="variable":
                if re.match(r"\w",char) != None:
                    tokenizer_temporary+=char

                else:
                    # restore one last character red
                    index-=1
                    type="variable"
                    tokenizer_state = "end"
            elif tokenizer_state=="string":
                if char=='"':
                    tokenizer_state="end"
                    type="string"

                elif re.match(r"[\w\s]",char) != None:
                    tokenizer_temporary+=char

            elif tokenizer_state=="comment":
                if char=='\n':
                    tokenizer_state="start"




            # while footer
            index+=1

        # after while
        token={}
        token["type"]=type
        token["value"]=tokenizer_temporary
        self.remained_file=self.remained_file[index:]
        self.last_token=token
        if len(self.stack)>4:
            del self.stack[0]

        self.stack.append(token)
        return token

    def can_read(self):
        next=self.read_token();
        self.undo_read()
        if next["value"]=="$":
            return False

        if len(self.remained_file)>1:
            return  True
        return False

    def undo_read(self):
        if len(self.stack )>0:

            self.remained_file=self.stack.pop()["value"]+" "+ self.remained_file
            self.last_token=self.stack[len(self.stack)-1]
        else:
            raise ValueError ("nothing to undo")


    def calculate(self):
        # L=> C backslashN L | landa
        # C=> t=e | e | print
        # e=> t+e | t-e | t |landa
        # t=> f*t |f/t | f
        # f=>id | (e)
        self.L()

    def L(self):
        print(self.C())
        if self.can_read():
            self.backslashN()
        if self.can_read():
            self.L()

    def backslashN(self):
        while self.read_token()["value"]=="\n":
            if self.can_read():
                pass
            else:
                return

        self.undo_read()


    def C(self):
        first_token=self.read_token()
        if first_token["value"]=="print":
            self.read_token() # (
            self.read_token() # print value
            ret=self.last_token["value"]
            self.read_token() #)
            return ret
        self.read_token()
        if self.last_token["value"] == "=":
            v_name=first_token["value"]
            self.token_set_value(first_token,self.E()["value"])
            import symbols
            print ("variable:"+first_token["value"]+ "="+  str(symbols.get_symbol_value(first_token["value"])))
            return first_token
        else:
            self.undo_read()
            self.undo_read()
            return self.E()

    def E(self):
        value=self.T()

        while (self.last_token["value"] == "+" or self.last_token["value"] == "-" ):
            if self.last_token["value"] == "+":
                val=self.T()["value"]
                value["value"]=float (value["value"]) +float(val)
            elif self.last_token["value"] == "-":
                val=self.T()["value"]
                value["value"]=float(value["value"]) -float(val)
        return value
    def T(self):
        value = self.F()
        self.read_token()
        while (self.last_token["value"] == "*" or self.last_token["value"] == "/" ):
            if self.last_token["value"] == "*":
                val=self.F()["value"]
                value["value"]*=float(value["value"]) *float(val)
            else:
                val=self.F()["value"]
                value["value"] =float(value["value"]) /float(val)
        return value


    def F(self):
        token=self.read_token()
        if token["type"]=="(":
            res = self.E()
            self.read_token() # read remained )
            return res
        elif token["type"]=="int" or token["type"]=="real":
            return token
        elif token["value"]=="input":
            self.read_token() # (
            self.read_token() # )
            temp_token={}
            temp_token["type"]="real"
            temp_token["value"]=input("value:")
            return temp_token

        elif token["type"]=="variable":
            return self.token_get_value(token)
        #todo else if not ( (wronge input)



    def token_get_value(self,token):
        if token["type"]=="int" or token["type"]=="real":
            return  token["value"]
        elif token["type"]=="variable":

            import symbols
            token["value"] =symbols.get_symbol_value(token["value"])
            return token

    def token_set_value(self,token,value):
        if token["type"]=="int" or token["type"]=="real":
            print("we cant set value into int or real")
        elif token["type"]=="variable":
            import symbols
            if symbols.exist_symbol(token["value"]):
                if symbols.get_symbol_type(token["value"]) =="int" or symbols.get_symbol_type(token["value"]) =="real":
                                symbols.set_symbol(token["value"],value)
                else: # input or print
                    raise ValueError("you cant have a variable with reserved names")
            else:
                symbols.set_symbol(token["value"],value)



