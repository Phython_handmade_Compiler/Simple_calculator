__author__ = 'Mahdi'


def isReal(txt):
    try:
        float(txt)
        return True
    except:
        return False


def isInt(txt):
    try:
        int(txt)
        return True
    except:
        return False


def get_number_type(txt):
    if isInt(txt):return 'int'
    elif isReal(txt):return 'real'
    else: return "etc"

def get_value(element):
    import re
    if isReal(element):
        return  float(element)
    elif re.match(r"[a-zA-Z][\w]*",element)!=None:
        import symbols
        return float(symbols.get_symbol_value(element))
    return  float(element)
