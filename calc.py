__author__ = 'Mahdi'

import useful_methods
import symbols

print("symbols table :")
print (symbols.symbols)

def process(elements):
    # while has_operator(elements):
    res = process_seprator(elements)[0]
    if not useful_methods.isReal(res):
        print("wrong calc:", res)
    return res


def has_operator(elements):
    for element in elements:
        if element == "+" or element == "-" or element == "/" or element == "*" or element == "%"or element == "^":
            return True
    return False


# def process_operator(elements):
# ;

def process_seprator(elements):
    # for i in range(1, len(elements) - 1):

    print("processing:",elements)

    if len(elements) == 1:
       return [useful_methods.get_value(elements)]


    prantis_open_index = -1
    prantis_close_index = -1
    for i in range(0, len(elements) ):
        if elements[i] == '(':
            prantis_open_index = i
            prantis_close_index=-1
        if elements[i] == ')' and prantis_close_index==-1:
            prantis_close_index  = i
    if int(prantis_open_index) > -1 and int(prantis_close_index ) > int(prantis_open_index):
        prantis = process_seprator(elements[prantis_open_index+1:prantis_close_index ])
        for j in range( prantis_close_index ,prantis_open_index ,-1):
            del elements[j]
        elements[prantis_open_index] = prantis[0]
        return process_seprator(elements)


    for i in range(1, len(elements) - 1):
            if elements[i] == '=':
                if symbols.exist_symbol(elements[0:i]):
                    if symbols.get_symbol_type(''.join(elements[0:i]))!='reserve' :
                        element_after_=process_seprator(''.join(elements[i + 1:]))
                        symbols.set_symbol(''.join(elements[0:i]),useful_methods.get_number_type(element_after_ ),element_after_ )
                else:
                    raise ValueError(elements[0:i], '  does not exist in symbols table')
                return [''.join(elements[i + 1:])]

    for i in range(1, len(elements) - 1):
        if elements[i] == '+':
            return processor(elements[0:i], elements[i], elements[i + 1:])
        if elements[i] == '-':
            return processor(elements[0:i], elements[i], elements[i + 1:])

    for i in range(1, len(elements) - 1):
        if elements[i] == '*':
            return processor(elements[0:i], elements[i], elements[i + 1:])
        if elements[i] == '/':
            return processor(elements[0:i], elements[i], elements[i + 1:])
        if elements[i] == '%':
            return processor(elements[0:i], elements[i], elements[i + 1:])
        if elements[i] == '^':
            return processor(elements[0:i], elements[i], elements[i + 1:])




def processor(elements_before, operator, elements_after):
    print("before:", elements_before, " operator:", operator, " after:", elements_after)
    if len(elements_before) == 1 and len(elements_after) == 1:
        import re
        if (useful_methods.isReal(elements_after[0]) or re.match(r"[a-zA-Z][\w]*",elements_after[0])!=None)\
                and (useful_methods.isReal(elements_before[0]) or  re.match(r"[a-zA-Z][\w]*",elements_before[0])!=None) :
            if operator == '*':
                return [useful_methods.get_value(elements_before[0]) * useful_methods.get_value(elements_after[0])]
            if operator == '/':
                return [useful_methods.get_value(elements_before[0]) / useful_methods.get_value(elements_after[0])]
            if operator == '%':
                return [useful_methods.get_value(elements_before[0]) % useful_methods.get_value(elements_after[0])]
            if operator == '+':
                return [useful_methods.get_value(elements_before[0]) + useful_methods.get_value(elements_after[0])]
            if operator == '-':
                return [useful_methods.get_value(elements_before[0]) - useful_methods.get_value(elements_after[0])]
            if operator == '^':
                return [useful_methods.get_value (elements_before[0]) ** useful_methods.get_value(elements_after[0])]

        else:
            print("we need digit to process. checkers are wrong")
            return False
    else:
        return processor(process_seprator(elements_before), operator, process_seprator(elements_after))


