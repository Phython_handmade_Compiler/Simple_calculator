__author__ = 'Mahdi'


# //symbols

symbols = {}
import useful_methods

###########################

#add something to the symbols
def assign(name,value,type):
    if not exist_symbol(name):

        if useful_methods.isReal(value ):
            set_symbol(name,'real',value)
        elif useful_methods.isInt(value):
            set_symbol(name,'int',value)
        else:
            set_symbol(name,type,value)

    else:
        raise ValueError(name , ' already exist in symbols table')


#true if something exist in symbols
def exist_symbol(symbol_name):
    if ''.join(symbol_name) in symbols:
        return True
    return False

#set exact data to symbol
def set_symbol(symbol_name,symbol_value):
    symbols[symbol_name]={}

    if useful_methods.isInt(symbol_value ):
           symbols[symbol_name]['type']="int"
    elif useful_methods.isReal(symbol_value):
           symbols[symbol_name]['type']="real"

    symbols[symbol_name]['value']=symbol_value

#read symbol by name
def get_symbol(symbol_name):
    if symbol_name in symbols:
        return symbols[symbol_name]
    else:
        raise ValueError(symbol_name,' does not exist in symbols list')

#read symbol value by name
def get_symbol_value(symbol_name):
    if symbol_name in symbols:
        return symbols[symbol_name]['value']
    else:
        raise ValueError(symbol_name,' does not exist in symbols list')

#read symbol type by name
def get_symbol_type(symbol_name):
    if symbol_name in symbols:
        return symbols[symbol_name]['type']
    else:
        raise ValueError(symbol_name,' does not exist in symbols list')

