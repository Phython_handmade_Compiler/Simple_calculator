__author__ = 'Mahdi'
import useful_methods

def practices_checker(array_inputs):
    number = 0
    for element in array_inputs:

        if element == '(':
            number += 1
        if element == ')':
            number -= 1
        if(number<0):
            print("practices do not match cant start using )")
            return False
    if number != 0:
        print("practices do not match we need more ", ')' if number > 0 else '(')
        return False

    for i in range(0, len(array_inputs) - 1):  # prevent () or (()) or etc
        if array_inputs[i] == ')':
            if array_inputs[i + 1] == '(':
                print("we do not allow )( at parameter ", i + 1)
                return False
        if array_inputs[i] == '(':
            if array_inputs[i + 1] == ')':
                print("we do not allow () at parameter ", i + 1)
                return False
    return True


def operator_input_checker(array_input):
    import re
    for i in range(0, len(array_input)):
        if (array_input[i] == '+' or array_input[i] == '-' or array_input[i] == '*' or array_input[i] == '/' or
                    array_input[i] == '%' or array_input[i] == '^'):
            if i == 0 or i == len(array_input) - 1:
                print('operator"', array_input[i], '" is at the beginning or ending')
                return False
            if not useful_methods.isReal(array_input[i - 1]) and not array_input[i - 1] == ')' and   re.match(r"[a-zA-Z][\w]*",array_input[i-1]) ==None:
                print('before parameter:', i, '"', array_input[i], '" we should have digit or )')
                return False
            if not useful_methods.isReal(array_input[i + 1]) and not array_input[i + 1] == '(' and   re.match(r"[a-zA-Z][\w]*",array_input[i+1]) ==None:
                print('after parameter:', i + 1, '"', array_input[i], '" we should have digit or (')
                return False
    return True


def digit_input_checker(array_input):
    for i in range(0, len(array_input)):
        if useful_methods.isReal(array_input[i]):
            if 0 < i:

                # prevent another digit near
                if useful_methods.isReal(array_input[i - 1]):
                    print('before parameter:', i, '"', array_input[i], '" we should not have digit')
                    return False
               # prevent ) before
                if array_input[i - 1] == ')':
                    print('before parameter:', i, '"', array_input[i], '" we should not have )')
                    return False


            if i < len(array_input) - 1:
                # prevent ( after
                 if array_input[i + 1] == '(':
                    print('after parameter:', i + 1, '"', array_input[i], '" we should not have (')
                    return False

                # prevent another digit near
                 if useful_methods.isReal(array_input[i + 1]):
                    print('after parameter:', i + 1, '"', array_input[i], '" we should not have digit')
                    return False

    return True


def is_valid_input(array_inputs):
    if not practices_checker(array_inputs):
        print("wrong practices")
        return False
    if not operator_input_checker(array_inputs):
        print("wrong operator")
        return False
    if not digit_input_checker(array_inputs):
        print("wrong digit")
        return False

    return True