__author__ = 'Mahdi'
class LL1:
    debug=True
    LLTable={}

    def __init__(self):
        # != Landa
        self.LLTable["E"]={}
        self.LLTable["E"]['(']=["T","L"]
        self.LLTable["E"]['a']=["T","L"]

        self.LLTable["F"]={}
        self.LLTable["F"]['(']=["(","E",")"]
        self.LLTable["F"]['a']=["a"]

        self.LLTable["L"]={}
        self.LLTable["L"][')']=["!"]
        self.LLTable["L"]['+']=["+","T","L"]
        self.LLTable["L"]['-']=["-","T","L"]
        self.LLTable["L"]['$']=["!"]

        self.LLTable["M"]={}
        self.LLTable["M"][')']=["!"]
        self.LLTable["M"]['*']=["*","F","M"]
        self.LLTable["M"]['+']=["!"]
        self.LLTable["M"]['-']=["!"]
        self.LLTable["M"]['*']=["/","F","M"]
        self.LLTable["M"]['$']=["!"]

        self.LLTable["T"]={}
        self.LLTable["T"]['(']=["F","M"]
        self.LLTable["T"]['a']=["F","M"]

        #equals to =>
        #   (   )   *   +   -   /   a   $
        # E	TL						TL
        # F	(E)						a
        # L		!		+TL	-TL			!
        # M		!	*FM	!	!	/FM		!
        # T	FM						FM


        #end of LLTable defining

    def isMatch(self):
        stack=Stack()
        stack.push("E")

        tokenizer=teached_tokenizer()

        while(stack.size()>0 ):
            token=tokenizer.read_token()
            if self.debug:
                print("stack:",stack.items," token:",token["value"])

            if token["value"]==stack.peek() :
                if self.debug:
                    print( "match found:",stack.peek(),token["value"])
                stack.pop()
                continue
            elif  stack.peek()=="!":
                if self.debug:
                    print( "Ignoring :",stack.peek(),token["value"])
                tokenizer.undo_read()
                stack.pop()
                continue
            else:
                tokenizer.undo_read()


            if stack.peek() in self.LLTable: #stack fits to the table  (this may happen due to miss tabling)
                if token["value"] in self.LLTable[stack.peek()]: #valid grammar so do the job
                    stack_=stack.pop()
                    for element in reversed(self.LLTable[stack_][token["value"]]):
                        stack.push(element)



                else: #not valid grammer
                    if self.debug:
                        print(token["value"]," does not match ",self.LLTable[stack.peek()])
                        print("wrong grammar")
                    break
        if tokenizer.can_read()==False and stack.isEmpty():
            return "matched"






class teached_tokenizer():
    import re
    remained_file = ''
    last_token=None
    stack =[]

    def __init__(self):
        teached_tokenizer.read_file(self)

    def read_file(self):

        # Open a file
        # fo = open("foo.txt", "r") #https://www.tutorialspoint.com/python/python_files_io.htm
        self.remained_file= ""
        with open("input.txt", "r") as ins:

            for line in ins:
                self.remained_file+=(line)
        self.remained_file += "$"
        print("file :", self.remained_file)

    def read_token(self):
        import re
        tokenizer_temporary=""
        tokenizer_state="start"

        index=0
        type="not done"
        while(tokenizer_state!="end"):
            char = self.remained_file[index]
            if tokenizer_state == "start":

                if char == '+':
                    type = "operator"
                    tokenizer_temporary = "+"
                    tokenizer_state="end"

                elif char == '-':
                    type = "operator"
                    tokenizer_temporary = "-"
                    tokenizer_state="end"

                elif char == '*':
                    type = "operator"
                    tokenizer_temporary = "*"
                    tokenizer_state="end"

                elif char == '/':
                    type = "operator"
                    tokenizer_temporary = "/"
                    tokenizer_state="end"

                elif char == '^':
                    type = "operator"
                    tokenizer_temporary = "^"
                    tokenizer_state="end"

                elif char == '%':
                    type = "operator"
                    tokenizer_temporary = "%"
                    tokenizer_state="end"

                elif char == '(':
                    type = "("
                    tokenizer_temporary = "("
                    tokenizer_state="end"

                elif char == ')':
                    type = ")"
                    tokenizer_temporary = ")"
                    tokenizer_state="end"

                elif char == '=':
                    type = "operator"
                    tokenizer_temporary = "="
                    tokenizer_state="end"

                elif char == '$':
                    type = "EOF"
                    tokenizer_temporary = "$"
                    tokenizer_state="end"
                elif char == '\n':
                    type = "newline"
                    tokenizer_temporary = "\n"
                    tokenizer_state="end"

                elif char == ' ':
                    pass

                elif char.isdigit() and 9 >= int(char) >= 0:
                    tokenizer_temporary += char
                    tokenizer_state = "int"

                elif re.match("[a-zA-Z]",char) != None:
                    tokenizer_temporary += char
                    tokenizer_state="variable"

                elif char=='"':
                    tokenizer_state="string"

                elif char=='#':
                    tokenizer_state="comment"

                else:
                    return "unsupported char"
            elif tokenizer_state == "int":
                if  char.isdigit() and 9 >= int(char) >= 0:
                    tokenizer_temporary += char
                    tokenizer_state = "int"

                elif char == '.':
                    tokenizer_state = "real"
                    tokenizer_temporary += char

                else:
                    # restore one last character red
                    index-=1
                    type="int"
                    tokenizer_state = "end"

            elif tokenizer_state == "real":
                if 9 >= int(char) >= 0:
                    tokenizer_temporary += char

                else:
                    # restore one last character red
                    index-=1
                    type="real"
                    tokenizer_state = "end"

            elif tokenizer_state=="variable":
                if re.match(r"\w",char) != None:
                    tokenizer_temporary+=char

                else:
                    # restore one last character red
                    index-=1
                    type="variable"
                    tokenizer_state = "end"
            elif tokenizer_state=="string":
                if char=='"':
                    tokenizer_state="end"
                    type="string"

                elif re.match(r"[\w\s]",char) != None:
                    tokenizer_temporary+=char

            elif tokenizer_state=="comment":
                if char=='\n':
                    tokenizer_state="start"




            # while footer
            index+=1

        # after while
        token={}
        token["type"]=type
        token["value"]=tokenizer_temporary
        self.remained_file=self.remained_file[index:]
        self.last_token=token
        if len(self.stack)>4:
            del self.stack[0]

        self.stack.append(token)
        return token

    def can_read(self):
        next=self.read_token();
        self.undo_read()
        if next["value"]=="$":
            return False

        if len(self.remained_file)>1:
            return  True
        return False

    def undo_read(self):
        if len(self.stack )>0:

            self.remained_file=self.stack.pop()["value"]+" "+ self.remained_file
            if len(self.stack)>0:
                self.last_token=self.stack[len(self.stack)-1]
            else:
                self.last_token="BOF"
        else:
            raise ValueError ("nothing to undo")



class Stack:
     def __init__(self):
         self.items = []

     def isEmpty(self):
         return self.items == []

     def push(self, item):
         self.items.append(item)

     def pop(self):
         return self.items.pop()

     def peek(self):
         return self.items[len(self.items)-1]

     def size(self):
         return len(self.items)